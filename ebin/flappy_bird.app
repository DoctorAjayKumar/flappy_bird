{application,flappy_bird,
             [{description,"Clone of Flappy Bird"},
              {registered,[]},
              {included_applications,[]},
              {applications,[stdlib,kernel]},
              {vsn,"0.1.0"},
              {modules,[fb_bird,fb_con,fb_gui,fb_sprites,fb_sup,fb_tix,flappy_bird]},
              {mod,{flappy_bird,[]}}]}.
