% bird x coordinate is fixed
-define(birdX, 35).
-define(birdYDefault, 200).
-define(birdCoordDefault, {?birdX, 200}).

-define(screenH, 512).
-define(baseH, 112).
-define(baseCoordXDef, 0).
-define(baseCoordYDef, ?screenH - ?baseH).
-define(baseCoordDefault, {?baseCoordXDef, ?baseCoordYDef}).

% ticks for base and pipes (need to move at same speed)
-define(envTickMS, 10).
% pixels per tick
-define(envVel, -1).

-type status() :: init | play | game_over.
