-module(fb_collision).

-export([check/1]).


-type bounding_box() :: {{x, integer(), integer()},
                         {y, integer(), integer()}}.
-type checkarg()     :: #{bird_bb  := bounding_box(),
                          base_bb  := bounding_box(),
                          pipe_bbs := [bounding_box()]}.


-spec check(checkarg()) -> boolean().
check(#{bird_bb := BirdBB, base_bb := BaseBB, pipe_bbs := PipeBBs}) ->
    collision_ceil(BirdBB) orelse
        collision_base(BirdBB, BaseBB) orelse
        collision_pipes(BirdBB, PipeBBs).


% collision with the ceiling when Y < 0
collision_ceil({_, {y, YMin, _}}) ->
    YMin < 0.


collision_base({_, {y, _, BirdYMax}}, {_, {y, BaseYMin, _}}) ->
    BaseYMin < BirdYMax.


collision_pipes(BirdBB, PipeBBs) ->
    Pred = fun(PipeBB) -> collision_pipe(BirdBB, PipeBB) end,
    lists:any(Pred, PipeBBs).


collision_pipe(BirdBB = {BirdXBounds, BirdYBounds},
               PipeBB = {PipeXBounds, PipeYBounds}) ->
    intersection(BirdXBounds, PipeXBounds) andalso intersection(BirdYBounds, PipeYBounds).


intersection(Box1, Box2) ->
    % going to be easier to check for disjointedness
    %
    % Only two cases: 1 is strictly to the left of 2, or 1 is strictly to the
    % right of 2
    not disjoint(Box1, Box2).


disjoint({_, Min1, Max1}, {_, Min2, Max2}) ->
    % case: 1 is strictly left of 2
    Box1_StrictlyLeftOf_Box2 = Max1 < Min2,
    % case: 1 is strictly right of 2
    Box1_StrictlyRightOf_Box2 = Max2 < Min1,
    % result is the disjunction of the two cases
    Result = Box1_StrictlyLeftOf_Box2 orelse Box1_StrictlyRightOf_Box2,
    Result.
