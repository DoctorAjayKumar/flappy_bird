% for doing ticks
%
% assumes you're calling this from a gen_server, and it can receive a cast
% which is just 'tock'
-module(fb_tix).

-behavior(gen_server).
-export([set_reply_after_ms/2, tick/1, tick/2]).
-export([start_link/0, init/1]).
-export([handle_call/3, handle_cast/2, handle_info/2, terminate/2]).
-include("$zx_include/zx_logger.hrl").


% default: reply after 1 second
-define(defaultReplyAfterMS, 1000).
-record(s,
        {reply_after_ms = ?defaultReplyAfterMS :: pos_integer()}).



%%% api

set_reply_after_ms(TickerPid, ReplyAfterMS) ->
    gen_server:cast(TickerPid, {set_reply_after_ms, ReplyAfterMS}).


tick(TickerPid) ->
    tick(TickerPid, #{reply_with => tock}).


tick(TickerPid, #{reply_with := Term}) ->
    Self = self(),
    gen_server:cast(TickerPid, {tick, #{reply_to => Self, reply_with => Term}});
tick(TickerPid, Term) ->
    tick(TickerPid, #{reply_with => Term}).



%%% startup

start_link() ->
    gen_server:start_link(?MODULE, none, []).


init(none) ->
    State = #s{},
    {ok, State}.



%%% gen_server message handling callbacks

handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


handle_cast({set_reply_after_ms, ReplyAfterMS}, State) ->
    NewState = State#s{reply_after_ms = ReplyAfterMS},
    {noreply, NewState};
handle_cast({tick, #{reply_to   := ReplyTo,
                     reply_with := ReplyWith}},
            State = #s{reply_after_ms = ReplyAfterMS}) ->
    ok = do_tick(#{reply_to       => ReplyTo,
                   reply_after_ms => ReplyAfterMS,
                   reply_with     => ReplyWith}),
    {noreply, State};
handle_cast(Unexpected, State) ->
    ok = log(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


terminate(Reason, State) ->
    ok = log(info, "Reason: ~tp, State: ~tp", [Reason, State]).



%%% internal
do_tick(_M = #{reply_to       := ReplyTo,
               reply_after_ms := ReplyAfterMS,
               reply_with     := ReplyWith}) ->
    ok = timer:sleep(ReplyAfterMS),
    ok = gen_server:cast(ReplyTo, ReplyWith),
    ok.

% debugging
println(Line) -> io:format("~p: ~p~n", [?MODULE, Line]).
