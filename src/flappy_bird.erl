-module(flappy_bird).

-export([start/2, stop/1]).


start(normal, _Args) ->
    %ok = observer:start(),
    fb_sup:start_link().


stop(_State) ->
    ok.
