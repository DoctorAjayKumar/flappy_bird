-module(fb_sup).

-export([start_link/0]).
-export([init/1]).


-spec start_link() -> {ok, pid()}.
%% @private
%% This supervisor's own start function.

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).


-spec init([]) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
%% @private
%% The OTP init/1 function.

init([]) ->
    SupFlags = #{strategy => one_for_one,
                 intensity => 0,
                 period => 60},
    BaseSpec = #{id => fb_base,
                 start => {fb_base, start_link, []}},
    BirdSpec = #{id => fb_bird,
                 start => {fb_bird, start_link, []}},
    PipesSpec = #{id => fb_pipes,
                  start => {fb_pipes, start_link, []}},
    ControllerSpec = #{id => fb_con,
                       start => {fb_con, start_link, []}},
    ChildSpecs  = [BaseSpec, BirdSpec, PipesSpec, ControllerSpec],
    {ok, {SupFlags, ChildSpecs}}.
