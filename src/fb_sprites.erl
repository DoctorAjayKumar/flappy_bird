-module(fb_sprites).

-include_lib("wx/include/wx.hrl").

-export([sprites/0]).

-type sprites() :: #{base         => wx:wxBitmap(),
                     bg           => wx:wxBitmap(),
                     bird_up      => wx:wxBitmap(),
                     bird_neutral => wx:wxBitmap(),
                     bird_down    => wx:wxBitmap(),
                     pipe_lower   => wx:wxBitmap(),
                     pipe_upper   => wx:wxBitmap()}.

sprites() ->
    #{base         => base(),
      bg           => bg(),
      bird_up      => bird_up(),
      bird_neutral => bird_neutral(),
      bird_down    => bird_down(),
      pipe_lower   => pipe_lower(),
      pipe_upper   => pipe_upper()}.


base()         -> pngload("base2.png").
bg()           -> pngload("bg.png").
bird_up()      -> pngload("bird_up.png").
bird_neutral() -> pngload("bird_neutral.png").
bird_down()    -> pngload("bird_down.png").
pipe_lower()   -> pngload("pipe_lower.png").
pipe_upper()   -> pngload("pipe_upper.png").


pngload(Filename) ->
    ImgDir = filename:join(zx_daemon:get_home(), "imgs"),
    PNG = [{type, ?wxBITMAP_TYPE_PNG}],
    FullFilename = filename:join(ImgDir, Filename),
    wxBitmap:new(FullFilename, PNG).
