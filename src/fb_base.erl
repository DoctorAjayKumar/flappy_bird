-module(fb_base).

%%% api
-export([coord/0, bounding_box/0]).
-export([play/0, game_over/0, reset/0]).

%%% behavior
-behavior(gen_server).
-export([start_link/0, init/1]).
-export([handle_call/3, handle_cast/2, handle_info/2]).
-include("$zx_include/zx_logger.hrl").


%%% type/state
-include("fb.hrl").
-define(baseTickMS, ?envTickMS).

%-define(bgW, 288).
%-define(bgH, 512).
%-define(baseW, 336).
%-define(baseH, 112).
%-define(baseCoordXDef, 0).
%-define(baseCoordYDef, 400).
% max coord is width - 1
-define(baseCoordXMax, 335).
-define(baseCoordYMax, 511).
% = basewidth - bgwidth
-define(tickMod, 48).

-record(s,
        {tix      = none :: none | pid(),
         offset_x = 0    :: -?tickMod..0,
         status   = init :: status()}).


%%% api

coord() ->
    gen_server:call(?MODULE, coord).


bounding_box() ->
    gen_server:call(?MODULE, bounding_box).


% play does nothing to the base, but it's here because fuck you
play() ->
    ok.


game_over() ->
    gen_server:cast(?MODULE, game_over).


% reset says set status to init
reset() ->
    gen_server:cast(?MODULE, reset).


%%% startup

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


init(none) ->
    {ok, Tix} = fb_tix:start_link(),
    ok = fb_tix:set_reply_after_ms(Tix, ?baseTickMS),
    ok = fb_tix:tick(Tix),
    State = #s{tix=Tix},
    {ok, State}.


%%% message handling

handle_call(coord, _From, State) ->
    Coord = do_coord(State),
    {reply, Coord, State};
handle_call(bounding_box, _From, State) ->
    Coords = do_bounding_box(),
    {reply, Coords, State};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


% reset just sets the status to init
handle_cast(reset, State) ->
    NewState = State#s{status = init},
    {noreply, NewState};
handle_cast(game_over, State) ->
    NewState = State#s{status = game_over},
    {noreply, NewState};
handle_cast(tock, State) ->
    NewState = do_tock(State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = tell(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.



%%% internals

do_coord(#s{offset_x=OffsetX}) ->
    X = ?baseCoordXDef + OffsetX,
    Y = ?baseCoordYDef,
    {X, Y}.


% bounding_box is for collision detection
do_bounding_box() ->
    {{x, ?baseCoordXDef, ?baseCoordXMax},
     {y, ?baseCoordYDef, ?baseCoordYMax}}.


%% If we're playing, then update
% status is game_over, send back a tick because why not
do_tock(State = #s{status = game_over, tix = Tix}) ->
    ok = fb_tix:tick(Tix),
    State;
% eh, no, base always moving, unless the game is over
do_tock(State = #s{status = _, tix = Tix, offset_x = T}) ->
    %ok = tell(info, "tick, state: ~w", [State]),
    ok = fb_tix:tick(Tix),
    NewOffset = (T + ?envVel) rem ?tickMod,
    NewState  = State#s{offset_x = NewOffset},
    NewState.
