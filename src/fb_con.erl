-module(fb_con).


%%% behavior and exports
-behavior(gen_server).
-export([spacebar/0, collision/0, key_n/0]).
-export([incscore/0]).
-export([start_link/0, stop/0]).
-export([init/1, terminate/2,
         handle_call/3, handle_cast/2, handle_info/2]).
-include("$zx_include/zx_logger.hrl").



%%% Type and Record Definitions

-define(gameTickMS, 5).
-include("fb.hrl").
-record(s,
        {window       = none              :: none | wx:wx_object(),
         tix          = none              :: none | pid(),
         wings        = down              :: down | up | neutral,
         base_coord   = ?baseCoordDefault :: coord(),
         bird_coord   = ?birdCoordDefault :: coord(),
         bird_angle_r = 0.0               :: float(),
         pipe_coords  = []                :: pipe_coords(),
         status       = init              :: status(),
         score        = 0                 :: non_neg_integer(),
         high_score   = 0                 :: non_neg_integer()}).
-type coord()       :: {non_neg_integer(), non_neg_integer()}.
-type pipe_coords() :: [fb_pipe:pipe_coord()].
%-type state() :: #s{}.


%%% api
spacebar() ->
    gen_server:cast(?MODULE, spacebar).


key_n() ->
    %println(?LINE),
    gen_server:cast(?MODULE, key_n).


collision() ->
    gen_server:cast(?MODULE, collision).


incscore() ->
    gen_server:cast(?MODULE, incscore).


%%% interface

stop() ->
    gen_server:cast(?MODULE, stop).


start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


init(none) ->
    ok = log(info, "Starting"),
    Window = fb_gui:start_link("Flappy Bird"),
    {ok, GameTix} = fb_tix:start_link(),
    ok = fb_tix:set_reply_after_ms(GameTix, ?gameTickMS),
    ok = fb_tix:tick(GameTix),
    ok = log(info, "Window: ~p", [Window]),
    State = #s{window = Window, tix = GameTix},
    {ok, State}.



%%% gen_server

handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


handle_cast(key_n, State) ->
    %println(?LINE),
    NewState = do_key_n(State),
    {noreply, NewState};
handle_cast(incscore, State = #s{score = Score, high_score = HighScore}) ->
    NewScore = Score + 1,
    NewHighScore = lists:max([NewScore, HighScore]),
    NewState = State#s{score = Score + 1, high_score = NewHighScore},
    {noreply, NewState};
handle_cast(collision, State) ->
    NewState = do_collision(State),
    {noreply, NewState};
handle_cast(spacebar, State) ->
    NewState = do_spacebar(State),
    {noreply, NewState};
handle_cast(tock, State) ->
    {ok, NewState} = do_tock(State),
    {noreply, NewState};
handle_cast(stop, State) ->
    ok = log(info, "Received a 'stop' message."),
    {stop, normal, State};
handle_cast(Unexpected, State) ->
    ok = tell(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


terminate(Reason, State) ->
    ok = tell(info, "Reason: ~tp, State: ~tp", [Reason, State]),
    zx:stop().



%%% private


% key n:
%
%   game over: new game
%   otherwise: ignore
%
% new game: save the window, tix, and (later) high score. everything else gets
% reset
do_key_n(_State = #s{status = game_over, window = W, tix = T, high_score = H}) ->
    %println(?LINE),
    % reset everyone
    ok = fb_base:reset(),
    ok = fb_bird:reset(),
    ok = fb_gui:reset(),
    ok = fb_pipes:reset(),
    NewState = #s{window = W, tix = T, high_score = H},
    NewState;
% otherwise, ignore
do_key_n(State = #s{status = _}) ->
    %println(?LINE),
    State.


% collision means the game is over
do_collision(State) ->
    ok = fb_base:game_over(),
    ok = fb_bird:game_over(),
    ok = fb_gui:game_over(),
    ok = fb_pipes:game_over(),
    State#s{status = game_over}.


% If we're in the init phase and spacebar is hit, tell everyone to play
%
% Everyone = base, bird, gui, pipes
do_spacebar(State = #s{status = init}) ->
    ok = fb_base:play(),
    ok = fb_bird:play(),
    ok = fb_gui:play(),
    ok = fb_pipes:play(),
    % also boost the bird
    ok = fb_bird:boost(),
    NewState = State#s{status = play},
    NewState;
% If we're playing and we hit the spacebar, just tell the bird to boost
do_spacebar(State = #s{status = play}) ->
    ok = fb_bird:boost(),
    State;
% If the game is over, do nothing
do_spacebar(State = #s{status = game_over}) ->
    State.


do_tock(State = #s{tix = GameTix}) ->
    % send back a tick
    ok = fb_tix:tick(GameTix),
    % collision checking
    ok = do_collision_check(State),
    % updating state
    Wings      = fb_bird:wings(),
    BirdCoord  = fb_bird:coord(),
    Radians    = fb_bird:angle_r(),
    BaseCoord  = fb_base:coord(),
    PipeCoords = fb_pipes:pipe_coords(),
    NewState   = State#s{wings        = Wings,
                         bird_coord   = BirdCoord,
                         bird_angle_r = Radians,
                         base_coord   = BaseCoord,
                         pipe_coords  = PipeCoords},
    ok = update_gui_state(NewState),
    %ok = tell(info, "~p:~p: NewState = ~w", [?MODULE, ?LINE, NewState]),
    {ok, NewState}.


update_gui_state(_tate = #s{wings        = Wings,
                            bird_coord   = BirdCoord,
                            bird_angle_r = Radians,
                            base_coord   = BaseCoord,
                            pipe_coords  = PipeCoords,
                            score        = Score,
                            high_score   = HighScore}) ->
    ok = fb_gui:set_wings(Wings),
    ok = fb_gui:set_bird_coord(BirdCoord),
    ok = fb_gui:set_bird_angle_r(Radians),
    ok = fb_gui:set_base_coord(BaseCoord),
    ok = fb_gui:set_pipe_coords(PipeCoords),
    ok = fb_gui:set_score(Score),
    ok = fb_gui:set_high_score(HighScore),
    ok.


do_collision_check(_State) ->
    BirdBB  = fb_bird:bounding_box(),
    BaseBB  = fb_base:bounding_box(),
    PipeBBs = fb_pipes:bounding_boxes(),
    ThereIsCollision = fb_collision:check(#{bird_bb  => BirdBB,
                                            base_bb  => BaseBB,
                                            pipe_bbs => PipeBBs}),
    ok = maybe_send_collision_msg(ThereIsCollision),
    ok.


% If no collision
maybe_send_collision_msg(_ThereIsCollision = false) ->
    ok;
maybe_send_collision_msg(_ThereIsCollision = true) ->
    collision().

% debugging
%println(Line) -> io:format("~p: ~p~n", [?MODULE, Line]).
