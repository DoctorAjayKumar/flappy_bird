-module(fb_gui).


%%% behavior and exports

-behavior(wx_object).
-include_lib("wx/include/wx.hrl").
-export([redraw/0, set_wings/1, set_bird_coord/1, set_bird_angle_r/1,
         set_base_coord/1, set_pipe_coords/1, set_high_score/1]).
-export([play/0, game_over/0, set_score/1, reset/0]).
-export([start_link/1, init/1]).
-export([terminate/2, handle_call/3, handle_cast/2,
         handle_info/2, handle_event/2]).
-include("$zx_include/zx_logger.hrl").
-include("fb.hrl").


%%% types/macros

-define(bgSize, {288, 512}).
-define(guiTickMS, 30).
-define(keySpace, 32).
-define(key_n, 78).
-define(scoreCoord, {10, 430}).
-define(fontSize, 12).
-define(black, {  0,   0,   0}).
-define(white, {255, 255, 255}).
-record(s,
        {frame        = none                 :: none | wx:wx_object(),
         panel        = none                 :: none | wx:wx_object(),
         wings        = down                 :: down | up | neutral,
         base_coord   = {0, 400}             :: coord(),
         bird_coord   = ?birdCoordDefault    :: coord(),
         bird_angle_r = 0.0                  :: float(),
         pipe_coords  = []                   :: pipe_coords(),
         tix          = none                 :: none | pid(),
         status       = init                 :: status(),
         score        = 0                    :: non_neg_integer(),
         high_score   = 0                    :: non_neg_integer(),
         sprites      = fb_sprites:sprites() :: fb_sprites:sprites()}).

-type coord()       :: {non_neg_integer(), non_neg_integer()}.
-type pipe_coords() :: [fb_pipe:pipe_coord()].


%%% api

redraw() ->
    gen_server:cast(?MODULE, redraw).


set_wings(Wings) ->
    gen_server:cast(?MODULE, {set_wings, Wings}).


set_bird_angle_r(Radians) ->
    gen_server:cast(?MODULE, {set_bird_angle_r, Radians}).


set_bird_coord(Coord) ->
    gen_server:cast(?MODULE, {set_bird_coord, Coord}).


set_base_coord(Coord) ->
    gen_server:cast(?MODULE, {set_base_coord, Coord}).


set_pipe_coords(Coord) ->
    gen_server:cast(?MODULE, {set_pipe_coords, Coord}).


set_score(Score) ->
    gen_server:cast(?MODULE, {set_score, Score}).


set_high_score(Score) ->
    gen_server:cast(?MODULE, {set_high_score, Score}).


play() ->
    gen_server:cast(?MODULE, play).


game_over() ->
    gen_server:cast(?MODULE, game_over).


reset() ->
    gen_server:cast(?MODULE, reset).


%%% startup

start_link(Title) ->
    wx_object:start_link({local, ?MODULE}, ?MODULE, Title, []).


init(Title) ->
    ok = log(info, "GUI starting..."),
    % create frame
    Wx = wx:new(),
    Frame = wxFrame:new(Wx, ?wxID_ANY, Title),
    % add panel
    Panel = wxPanel:new(Frame),
    ok = wxFrame:setClientSize(Frame, ?bgSize),
    % connect to close_window, char_hook, key_up and key_down
    ok = wxFrame:connect(Frame, close_window),
    ok = wxFrame:connect(Frame, char_hook),
    %ok = wxFrame:connect(Frame, key_down),
    %ok = wxFrame:connect(Frame, key_up),
    % because tiling window manager
    ok = wxFrame:connect(Frame, size),
    % show the frame
    ok = wxFrame:center(Frame),
    true = wxFrame:show(Frame),
    % gui ticks for redrawing the screen
    {ok, Tix} = fb_tix:start_link(),
    ok = fb_tix:set_reply_after_ms(Tix, ?guiTickMS),
    ok = fb_tix:tick(Tix),
    % construct the state
    State = #s{frame = Frame,
               panel = Panel,
               tix   = Tix},
    {Frame, State}.



%%% gen_server

handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


% should move this case to a function but eh
% reset resets everything except the frame, the panel and the ticks
handle_cast(reset, _State = #s{frame = Frame, panel = Panel, tix = Tix}) ->
    NewState = #s{frame = Frame, panel = Panel, tix = Tix},
    {noreply, NewState};
% should move this case to a function but eh
handle_cast(game_over, State) ->
    NewState = State#s{status = game_over},
    {noreply, NewState};
% should move this case to a function but eh
handle_cast(play, State) ->
    NewState = State#s{status = play},
    {noreply, NewState};
handle_cast(tock, State) ->
    ok = do_tock(State),
    {noreply, State};
handle_cast(redraw, State) ->
    ok = redraw(State),
    {noreply, State};
handle_cast({set_wings, Wings}, State) ->
    NewState = do_set_wings(Wings, State),
    {noreply, NewState};
handle_cast({set_bird_angle_r, Radians}, State) ->
    NewState = do_set_bird_angle_r(Radians, State),
    {noreply, NewState};
handle_cast({set_bird_coord, Coord}, State) ->
    NewState = do_set_bird_coord(Coord, State),
    {noreply, NewState};
handle_cast({set_base_coord, Coord}, State) ->
    NewState = do_set_base_coord(Coord, State),
    {noreply, NewState};
handle_cast({set_pipe_coords, Coord}, State) ->
    NewState = do_set_pipe_coords(Coord, State),
    {noreply, NewState};
handle_cast({set_score, Score}, State) ->
    NewState = State#s{score = Score},
    {noreply, NewState};
handle_cast({set_high_score, Score}, State) ->
    NewState = State#s{high_score = Score},
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = tell(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


terminate(Reason, State) ->
    ok = tell(info, "Reason: ~tp, State: ~tp", [Reason, State]),
    wx:destroy().



%%% wx_object callbacks

handle_event(#wx{event = #wxClose{}}, State = #s{frame = Frame}) ->
    ok = fb_con:stop(),
    ok = wxWindow:destroy(Frame),
    {noreply, State};
% tiling wm annoying, redraw on resize
handle_event(#wx{event = #wxSize{}}, State) ->
    ok = redraw(State),
    {noreply, State};
handle_event(#wx{event = #wxKey{keyCode = ?keySpace}}, State) ->
    ok = do_spacebar(),
    {noreply, State};
handle_event(#wx{event = #wxKey{keyCode = ?key_n}}, State) ->
    ok = fb_con:key_n(),
    {noreply, State};
handle_event(Event, State) ->
    ok = tell(info, "Unexpected event ~tw State: ~tw~n", [Event, State]),
    {noreply, State}.



%%% private

do_spacebar() ->
    ok = fb_con:spacebar(),
    ok.

do_set_wings(Wings, State) ->
    NewState = State#s{wings=Wings},
    NewState.


do_set_bird_angle_r(Radians, State) ->
    NewState = State#s{bird_angle_r=Radians},
    NewState.


do_set_bird_coord(C, State) ->
    NewState = State#s{bird_coord=C},
    NewState.


do_set_base_coord(C, State) ->
    NewState = State#s{base_coord=C},
    NewState.


do_tock(State=#s{tix=Tix}) ->
    ok = fb_tix:tick(Tix),
    ok = redraw(State),
    ok.


do_set_pipe_coords(PipeCoords, State) ->
    %ok = tell(info, "new pipe coords: ~w", [PipeCoords]),
    NewState = State#s{pipe_coords=PipeCoords},
    NewState.


%resize(#s{frame=F}) ->
%    ok = wxFrame:setSize(F, ?bgSize),
%    ok.


redraw(State = #s{panel = Panel}) ->
    PanelDC = wxClientDC:new(Panel),
    DC = wxBufferedDC:new(PanelDC),
    ok = showbg(DC, State),
    ok = showpipes(DC, State),
    ok = showbase(DC, State),
    ok = showbird(DC, State),
    ok = showtext(DC, State),
    ok = wxBufferedDC:destroy(DC),
    ok = wxClientDC:destroy(PanelDC),
    ok.


showtext(DC, State) ->
    Text = the_text(State),
    Font = font(),
    ok = wxDC:setFont(DC, Font),
    ok = wxDC:setTextForeground(DC, ?black),
    ok = wxDC:drawText(DC, Text, ?scoreCoord),
    ok.


% what text to display
the_text(#s{status = init, high_score = HighScore}) ->
    Str = io_lib:format("HIT SPACEBAR TO PLAY~n"
                        "ONCE PLAYING, HIT SPACEBAR~nTO JUMP~n"
                        "HIGH SCORE: ~4.. B",
                        [HighScore]),
    Str;
the_text(#s{status = play, score = Score, high_score = HighScore}) ->
    Str = io_lib:format("HIT SPACEBAR TO JUMP~n"
                        "     SCORE: ~4.. B~n"
                        "HIGH SCORE: ~4.. B",
                        [Score, HighScore]),
    Str;
the_text(#s{status = game_over, score = Score, high_score = HighScore}) ->
    Str = io_lib:format("GAME OVER~n"
                        "FINAL SCORE: ~4.. B~n"
                        " HIGH SCORE: ~4.. B~n"
                        "TO PLAY AGAIN, PRESS n",
                        [Score, HighScore]),
    Str.



font() ->
    FontSize   = ?fontSize,
    FontFamily = ?wxFONTFAMILY_TELETYPE,
    FontStyle  = ?wxFONTSTYLE_NORMAL,
    FontWeight = ?wxFONTWEIGHT_BOLD,
    wxFont:new(FontSize, FontFamily, FontStyle, FontWeight).


showbg(DC, #s{sprites = #{bg := BgImg}}) ->
    ok = wxDC:drawBitmap(DC, BgImg, {0, 0}),
    ok.


showpipes(DC, #s{pipe_coords = PipeCoords, sprites = Sprites}) ->
    ok = showpipes_dc(DC, PipeCoords, Sprites),
    ok.


showbird(DC, #s{bird_angle_r = Radians, bird_coord = Coord, wings = Wings,
                sprites = Sprites}) ->
    ok = showbird_dc(DC, Radians, Coord, Wings, Sprites),
    ok.


showbase(DC, #s{base_coord=Coord, sprites=#{base := Bitmap}}) ->
    wxDC:drawBitmap(DC, Bitmap, Coord),
    ok.


showbird_dc(DC, Radians, Coord, down, #{bird_down := Bitmap}) ->
    wxDC:drawBitmap(DC, rotate(Bitmap, Radians), Coord);
showbird_dc(DC, Radians, Coord, up, #{bird_up := Bitmap}) ->
    wxDC:drawBitmap(DC, rotate(Bitmap, Radians), Coord);
showbird_dc(DC, Radians, Coord, neutral, #{bird_neutral := Bitmap}) ->
    wxDC:drawBitmap(DC, rotate(Bitmap, Radians), Coord).


showpipes_dc(_, [], _) ->
    ok;
showpipes_dc(DC, [{lower, Coord} | Rest], Sprites = #{pipe_lower := Sprite}) ->
    ok = wxDC:drawBitmap(DC, Sprite, Coord),
    %{X, _} = Coord,
    %ok = wxDC:drawBitmap(DC, Sprite, {X, 100}),
    showpipes_dc(DC, Rest, Sprites);
showpipes_dc(DC, [{upper, Coord} | Rest], Sprites = #{pipe_upper := Sprite}) ->
    ok = wxDC:drawBitmap(DC, Sprite, Coord),
    %{X, _} = Coord,
    %ok = wxDC:drawBitmap(DC, Sprite, {X - 50, 100}),
    showpipes_dc(DC, Rest, Sprites).


rotate(Bitmap, Radians) ->
    %ok = io:format("rotate ~p by ~p radians~n", [Bitmap, Radians]),
    Img = wxBitmap:convertToImage(Bitmap),
    NewImg = wxImage:rotate(Img, Radians, {0, 0}),
    NewBitmap = wxBitmap:new(NewImg),
    NewBitmap.


% debugging
%println(Line) -> io:format("~p: ~p~n", [?MODULE, Line]).
