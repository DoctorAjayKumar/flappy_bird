-module(fb_bird).

-behavior(gen_server).
-export([boost/0]).
-export([play/0, game_over/0, reset/0]).
-export([angle_r/0, coord/0, bounding_box/0, wings/0]).
-export([start_link/0, init/1]).
-export([handle_call/3, handle_cast/2, handle_info/2, terminate/2]).
-include("$zx_include/zx_logger.hrl").

% experimentally it seems, high terminal falling velocity is good, so is low
% absolute terminal climbing velocity. Mess with the amount of gravity I think
-define(wingTickMS,  80).
% mess with how long the boost lasts, and the terminal climbing velocity, -3.5
% boost acceleration is just right
-define(unboostMS, 100).
-define(boostAccel, -3.5).
% reduce the gravity, not the ticks (faster ticks = smoother)
-define(gTickMS, 25).
% 1 pixel per tock per tock
-define(gravity, 0.7).
% max angle in milliradians (about 23 degrees)
% 1 milliradian ~= 0.57 degrees
-define(maxAngleMR,  400).
-define(minAngleMR, -1400).
-define(flapThreshold, -400).
% minimum because climbing is negative velocity
-define(minClimbVelocity, -08.0).
-define(terminalVelocity,  30.0).
-include("fb.hrl").
-define(birdWidth, 34).
-define(birdHeight, 24).
% if it's 34 pixels wide, the rightmost pixel is at position 33
-define(maxXDiff, ?birdWidth - 1).
-define(maxYDiff, ?birdHeight - 1).
% 'tick' absent any qualification refers to the wing ticks
-record(s,
        {wings    = down          :: up | down | neutral,
         tix      = none          :: none | pid(),
         gtix     = none          :: none | pid(),
         tick     = 0             :: 0..3,
         x        = ?birdX        :: non_neg_integer(),
         y        = ?birdYDefault :: non_neg_integer(),
         v        = 0.0           :: float(),
         a        = ?gravity      :: float(),
         angle_mr = 0             :: ?minAngleMR..?maxAngleMR,
         status   = init          :: status()}).

%%% api

angle_r() ->
    gen_server:call(?MODULE, angle_r).


coord() ->
    gen_server:call(?MODULE, coord).


bounding_box() ->
    gen_server:call(?MODULE, bounding_box).


boost() ->
    gen_server:cast(?MODULE, boost).


wings() ->
    gen_server:call(?MODULE, wings).


play() ->
    gen_server:cast(?MODULE, play).


game_over() ->
    gen_server:cast(?MODULE, game_over).


reset() ->
    gen_server:cast(?MODULE, reset).


%%% startup

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


init(none) ->
    % wings ticks
    {ok, WingTix} = fb_tix:start_link(),
    ok = fb_tix:set_reply_after_ms(WingTix, ?wingTickMS),
    ok = fb_tix:tick(WingTix),
    % gravity ticks
    {ok, GTix} = fb_tix:start_link(),
    ok = fb_tix:set_reply_after_ms(GTix, ?gTickMS),
    ok = gtick(GTix),
    % return state
    State =
        #s{
            tix  = WingTix,
            gtix = GTix
        },
    {ok, State}.



%%% gen_server message handling

handle_call(coord, _From, State=#s{x=X, y=Y}) ->
    {reply, {X,Y}, State};
handle_call(bounding_box, _From, State) ->
    Coords = do_bounding_box(State),
    {reply, Coords, State};
handle_call(angle_r, _From, State=#s{angle_mr=AngleMR}) ->
    AngleR = AngleMR/1000,
    {reply, AngleR, State};
handle_call(wings, _From, State=#s{wings=W}) ->
    {reply, W, State};
handle_call(y, _From, State=#s{y=Y}) ->
    {reply, Y, State};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


% reset resets everything except the ticks
handle_cast(reset, State = #s{tix = Tix, gtix = GTix}) ->
    NewState = #s{tix = Tix, gtix = GTix},
    {noreply, NewState};
% should move this case to a function but eh
handle_cast(game_over, State) ->
    NewState = State#s{status = game_over},
    {noreply, NewState};
% should move this case to a function but eh
handle_cast(play, State) ->
    NewState = State#s{status = play},
    {noreply, NewState};
handle_cast(boost, State) ->
    {ok, NewState} = do_boost(State),
    {noreply, NewState};
handle_cast(gtock, State) ->
    {ok, NewState} = do_gtock(State),
    {noreply, NewState};
handle_cast(tock, State) ->
    {ok, NewState} = do_tock(State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = tell(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


handle_info(unboost, State) ->
    NewState = do_unboost(State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = tell(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


terminate(Reason, State) ->
    ok = log(info, "Reason: ~tp, State: ~tp", [Reason, State]).



%%% private

do_bounding_box(#s{x=X, y=Y}) ->
    BB = {{x, X, X + ?maxXDiff},
          {y, Y, Y + ?maxYDiff}},
    BB.


do_unboost(State) ->
    NewA = ?gravity,
    NewState = State#s{a=NewA},
    NewState.


do_boost(State) ->
    % send unboost tick
    _ = erlang:send_after(?unboostMS, self(), unboost),
    % set acceleration to boost
    NewA = ?boostAccel,
    NewState =
        State#s{
            a=NewA
        },
    {ok, NewState}.


% only gravity during play, otherwise no state mutation
do_gtock(State = #s{status = play, gtix = GTix, y = Y, v = V, a = A}) ->
    % send a tick back
    ok = gtick(GTix),
    % update velocity and position
    NewY       = round(Y + V),
    NewV       = update_velocity(V, A),
    NewAngleMR = update_angle_mr(NewV),
    NewState   = State#s{y        = NewY,
                         v        = NewV,
                         angle_mr = NewAngleMR},
    {ok, NewState};
% not play, just send back a tock
do_gtock(State = #s{status = _, gtix = GTix}) ->
    % send a tick back
    ok = gtick(GTix),
    {ok, State}.


% update angle using linear interpolation between minangle->maxvelo, and
% maxangle->minvelo
update_angle_mr(NewV) ->
    VeloPercentage = (NewV - ?minClimbVelocity) / (?terminalVelocity - ?minClimbVelocity),
    AngleDiff_Float = VeloPercentage*(?maxAngleMR - ?minAngleMR),
    AngleDiff_Int = round(AngleDiff_Float),
    % We subtract from the max angle because the orientation switches;
    % positive velocity -> downward
    % negative angle -> downward
    ?maxAngleMR - AngleDiff_Int.


% if we can't climb faster (climbing is negative velocity), use min climb
% velocity
update_velocity(V, A) when (V + A) < ?minClimbVelocity ->
    ?minClimbVelocity;
% if we can't descend faster (falling is positive velocity), use terminal
% velocity
update_velocity(V, A) when (V + A) > ?terminalVelocity ->
    ?terminalVelocity;
update_velocity(V, A) ->
    V + A.


% still flapping the wings during init or play, during game over do nothing
do_tock(State = #s{status = game_over, tix = Tix}) ->
    % send a tick back because why not
    ok = fb_tix:tick(Tix),
    {ok, State};
do_tock(State = #s{status = _, tix = Tix, tick = T, angle_mr = AngleMR}) ->
    % send a tick back
    ok = fb_tix:tick(Tix),
    % update tick and wings
    NewT = (T + 1) rem 4,
    NewWings = wingtock(AngleMR, NewT),
    NewState = State#s{tick  = NewT,
                       wings = NewWings},
    {ok, NewState}.


% so I don't forget to send the tock message
gtick(GTix) -> fb_tix:tick(GTix, gtock).


% If angle is greater than 100 milliradians, do periodic
wingtock(AngleMR, T) when AngleMR > ?flapThreshold ->
    wingtock(T);
% If angle is ge 100 milliradians, he's gliding, so neutral wings
wingtock(_, _) ->
    neutral.


% wings are periodic
wingtock(0) -> down;
wingtock(1) -> neutral;
wingtock(2) -> up;
wingtock(3) -> neutral.


% debugging
%println(Line) -> io:format("~p: ~p~n", [?MODULE, Line]).

