% 3 pairs of pipes, equidistant, heights randomly set
-module(fb_pipes).

-behavior(gen_server).
-export([pipe_coords/0, bounding_boxes/0]).
-export([play/0, game_over/0, reset/0]).
-export([start_link/0, init/1]).
-export([handle_call/3, handle_cast/2, handle_info/2]).
-export_type([pipe_coord/0]).
-include("$zx_include/zx_logger.hrl").

-include("fb.hrl").
-define(pipeTickMS, ?envTickMS).
-define(pipeGapX, 100).
-define(pipeGapY, 125).
-define(pipeW, 52).
-define(pipeH, 320).
-define(screenW, 288).
-define(pipeResetX, 550).
% parameters for randomly generating pipe height
-define(pipeYLowerBound, 150).
-define(pipeYRandRange,  200).
-record(s,
        {tix    = none         :: none | pid(),
         pairs  = init_pairs() :: [pair()],
         status = init         :: status()}).
% pair coord refers to the top left point of the lower pipe
-record(pair,
        {x                  :: integer(),
         y                  :: integer(),
         score_incd = false :: boolean()}).
-type state()      :: #s{}.
-type pipe_type()  :: lower | upper.
-type pipe_coord() :: {pipe_type(), coord()}.
-type pair()       :: #pair{}.
-type coord()      :: {integer(), integer()}.


%%% api

-spec pipe_coords() -> [pipe_coord()].
pipe_coords() ->
    gen_server:call(?MODULE, coords).


bounding_boxes() ->
    gen_server:call(?MODULE, bounding_boxes).


play() ->
    gen_server:cast(?MODULE, play).


game_over() ->
    gen_server:cast(?MODULE, game_over).


reset() ->
    gen_server:cast(?MODULE, reset).


%%% startup

-spec start_link() -> {ok, pid()}.
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec init(none) -> {ok, state()}.
init(none) ->
    % start tix
    {ok, Tix} = fb_tix:start_link(),
    ok = fb_tix:set_reply_after_ms(Tix, ?pipeTickMS),
    ok = fb_tix:tick(Tix),
    % make initial pairs
    State = #s{tix=Tix},
    {ok, State}.


%%% gen_server message handling callbacks

handle_call(coords, _From, State) ->
    Coords = do_coords(State),
    {reply, Coords, State};
handle_call(bounding_boxes, _From, State) ->
    BBs = do_bounding_boxes(State),
    {reply, BBs, State};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


% should move this case to a function but eh
% reset resets everything except the ticks
handle_cast(reset, _State = #s{tix = Tix}) ->
    NewState = #s{tix = Tix},
    {noreply, NewState};
% should move this case to a function but eh
handle_cast(game_over, State) ->
    NewState = State#s{status = game_over},
    {noreply, NewState};
% should move this case to a function but eh
handle_cast(play, State) ->
    NewState = State#s{status = play},
    {noreply, NewState};
handle_cast(tock, State) ->
    NewState = do_tock(State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = tell(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


%%% private fns

-spec do_coords(state()) -> [pipe_coord()].
do_coords(#s{pairs=Pairs}) ->
    pair_coords(Pairs, []).


pair_coords([], Accum) ->
    Accum;
pair_coords([Pair | Rest], Accum) ->
    NewCoords = coords_of_pair(Pair),
    NewAccum = NewCoords ++ Accum,
    pair_coords(Rest, NewAccum).


% if the pipes are out of frame, don't show them
% {X, Y} is the top left coord of the lower pipe
coords_of_pair(#pair{x=X}) when X < -?pipeW orelse X >= ?screenW ->
    [];
coords_of_pair(#pair{x=X, y=Y}) ->
    LowerCoord = {lower, {X, Y}},
    UpperY = Y - ?pipeGapY - ?pipeH,
    UpperCoord = {upper, {X, UpperY}},
    Result = [LowerCoord, UpperCoord],
    %ok = tell(info, "~w", [Result]),
    Result.


do_bounding_boxes(#s{pairs = Pairs}) ->
    lists:flatten([do_bounding_boxes_of_pair(Pair) || Pair <- Pairs]).


% {X, Y} is the top left coord of the lower pipe
do_bounding_boxes_of_pair(#pair{x=X, y=Y}) ->
    % Lower bounding box
    LowerBB_x = {x, X, X + ?pipeW},
    LowerBB_y = {y, Y, Y + ?pipeH},
    LowerBB = {LowerBB_x, LowerBB_y},
    % upper bounding box
    UpperTLX = X,
    UpperTLY = Y - ?pipeGapY - ?pipeH,
    UpperBB_x = {x, UpperTLX, UpperTLX + ?pipeW},
    UpperBB_y = {y, UpperTLY, UpperTLY + ?pipeH},
    UpperBB = {UpperBB_x, UpperBB_y},
    [LowerBB, UpperBB].



-spec do_tock(state()) -> state().
% only move the pipes if we're playing
do_tock(State = #s{status = play, tix = Tix, pairs = Pipes}) ->
    % send back a tick
    ok = fb_tix:tick(Tix),
    % move pairs
    NewPipes = mv_pairs(Pipes),
    NewState = State#s{pairs = NewPipes},
    NewState;
% not playing, send back a tick
do_tock(State = #s{status = _, tix = Tix}) ->
    % send back a tick
    ok = fb_tix:tick(Tix),
    State.


-spec mv_pairs([pair()]) -> [pair()].
mv_pairs(Pairs) ->
    mv_pairs(Pairs, []).


mv_pairs([], Accum) ->
    Accum;
mv_pairs([Pair | Rest], Accum) ->
    NewAccum = [mv_pair(Pair) | Accum],
    mv_pairs(Rest, NewAccum).


% if out of frame, move to reset position
mv_pair(Pair = #pair{x = X}) when X < -?pipeW ->
    Pair#pair{x          = ?pipeResetX,
              y          = rand_y(),
              score_incd = false};
% there's an intermediate stage where we've passed the bird, we want to
% increment the score, but only do it once
%
% If we haven't incremented the score, just do it once, don't update the state
% any other way, wait until the next tick to update the position
mv_pair(Pair = #pair{x = X, score_incd = false}) when X < ?birdX - ?pipeW ->
    ok = fb_con:incscore(),
    Pair#pair{score_incd = true};
% otherwise move by environment velocity
mv_pair(Pair = #pair{x = X}) ->
    NewX = X + ?envVel,
    Pair#pair{x = NewX}.


-spec init_pairs() -> [pair()].
init_pairs() ->
    % maybe these should be factored out into macros but meh
    [pair(300, rand_y()),
     pair(500, rand_y()),
     pair(700, rand_y())].


rand_y() ->
    Offset = rand:uniform(?pipeYRandRange),
    LowerBound = ?pipeYLowerBound,
    LowerBound + Offset.


-spec pair(integer(), integer()) -> pair().
pair(X, Y) ->
    #pair{x=X, y=Y}.


